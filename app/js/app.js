var app = (function(document, $) {

	'use strict';
	var docElem = document.documentElement,

		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_init = function() {
			$(document).foundation();
			_userAgentInit();

			window.globals = {
				controller: window.location.pathname,
				news_current: 0,
				news_selector: '#newsSliderContainer .captions ul li',
				nav_stick_on: document.querySelectorAll('div.main-nav')[0].offsetTop
			}

			$('#searchForm').find('a').bind('click', function(event) {
				event.preventDefault();
				$(this).parents('form').submit();
			});

			$('.programa-resposta')
				.siblings('.orbit-bullets-container')
					.prepend('<a href="#">mais vídeos</a>')

			$('a[data-custom-dropdown]').bind('click', function(event) {
				event.preventDefault();
				
				var index   = $(this).index('a[data-custom-dropdown]'),
					target  = $('#' + $(this).attr('data-custom-dropdown'));

				$.each($('a[data-custom-dropdown]'), function(i, val) {
					if(i !== index && $(this).hasClass('arrow-down')) {
						$(this).removeClass('arrow-down');

						$('#' + $(this).attr('data-custom-dropdown')).hide();
					};
				});

				$(this).toggleClass('arrow-down');

				if (target.is(':hidden')) {
					target.fadeIn('fast');
				} else {
					target.fadeOut('fast');
				}
			});

			$('.btnSearch').bind('click', function(event) {
				event.preventDefault();
				$(this).toggleClass('close');
				$('#searchForm').slideToggle('fast');
			});

			$('#offcanvasMenu').bind('click', function(event) {
				event.preventDefault();
				$(this).offcanvasNav({animateStickNav: true});
			});

			// content related behaviours
			if (globals.controller == '/' || globals.controller == '/wp/') {
				$(globals.news_selector).eq(0).show(); // init the news slider
				$('#newsSlider')
					.on('ready.fndtn.orbit', function(event) {
					})
					.on('before-slide-change.fndtn.orbit', function(event) {
						$(globals.news_selector).eq(globals.news_current).fadeOut('fast');
					})
					.on('after-slide-change.fndtn.orbit', function(event, index) {
						globals.news_current = index.slide_number;
						$(globals.news_selector).eq(globals.news_current).fadeIn('fast');
					});

				// $('.video-thumbs').slick({
				// 	// centerMode: true,
				// 	lazyLod: 'ondemand',
				// 	infinite: true,
				// 	slidesToShow: 4,
				// 	slidesToScroll: 2,
				// 	responsive: [
				// 		{
				// 			breakpoint: 641,
				// 			settings: {
				// 				centerMode: false,
				// 				slidesToShow: 1,
				// 				slidesToScroll: 1,
				// 				dots: true
				// 			}
				// 		}
				// 	]
				// });
				
				$('#pastores-slide').slick({
					lazyLod: 'ondemand',
					infinite: true,
					slidesToShow: 2,
					slidesToScroll: 1
				});
			};

			if (globals.controller.indexOf("ministerio") !== -1 || globals.controller.indexOf("pastor")) {
				$('.ministerios-slider').slick({
					dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 3,
					slidesToScroll: 2,
					lazyLoad: 'ondemand',
					responsive: [
						{
							breakpoint: 641,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								arrows: false,
								dots: true
							}
						}
					]
				});

				$('.btn-ministerios').hover(function() {
					$(this).find('div').fadeIn('fast')
				}, function() {
					$(this).find('div').fadeOut('fast')
				});

				$('.ministerios-slider')
					.find('.slick-prev').attr('data-arrow', 'G')
					.end()
					.find('.slick-next').attr('data-arrow', 'H');
			};
			
			// Contact page
			if (globals.controller.indexOf("contato") !== -1) {
				$.getScript( window.themeURL + "/js/vendor/jquery.mask.js", function(){
					var phoneMask = ['(00) 00000-0000', '(00) 0000-00009'];
					$('#telefone').mask(phoneMask[1], {onKeyPress: 
						function(val, e, field, options) {
							field.mask(val.length > 14 ? phoneMask[0] : phoneMask[1], options) ;
						}
					});
				});
				$('#cadastro').on('valid.fndtn.abide', function() {
					event.preventDefault();

					window.dataString = $(this).serialize();
					console.log(dataString);

					// AJAX Code To Submit Form.
					$.ajax({
						type: 'POST',
						url: window.themeURL + '/send-form.php',
						data: dataString,
						cache: false,
						success: function(result){
							console.log(result);
							$('#alert-success').css('display', 'block');
						},
						error: function(xhr, textStatus, errorThrown) {
							$('#alert-error').css('display', 'block');
						}
					});
					return false;
				});
			};
		};

	return {
		init: _init
	};

})(document, jQuery);

var utils = (function(document, $) {
	
	'use strict';
	$.fn.stickNav = function(options, callback) {
		var settings = $.extend(true, {
			top: 0,
			stickTo: 0,
			isSticked: false
		}, options);

		if ( $(window).scrollTop() >= settings.top ) {
			settings.isSticked = true;

			this.css({
				position: 'fixed',
				'backface-visibility': 'hidden',
				'box-shadow': 'rgba(0,0,0,.3) 0 10px 12px -8px',
				top: settings.stickTo,
				left: 0,
				right: 0
			});
		} else {
			this.removeAttr('style');
			settings.isSticked = false;
		}

		if (typeof callback == 'function') {
			callback.call(this, settings);
		};
	}

	$.fn.offcanvasNav = function(options, callback) {
		var settings = $.extend(true, {
			body: document.body,
			target: document.getElementById( 'cbp-spmenu-s2' ),
			animateStickNav: false

		}, options);

		this.toggleClass('active');
		// vanilla for settings.target.toggleClass('cbp-spmenu-open');
		if ( settings.target.className.match(/(?:^|\s)cbp-spmenu-open(?!\S)/) ) {
			settings.target.className = settings.target.className.replace( /(?:^|\s)cbp-spmenu-open(?!\S)/g , '' );
		} else {
			settings.target.className += " cbp-spmenu-open"
		}

		// vanilla for settings.body.toggleClass('cbp-spmenu-push-toleft');
		if ( settings.body.className.match(/(?:^|\s)cbp-spmenu-push-toleft(?!\S)/) ) {
			settings.body.className = settings.body.className.replace( /(?:^|\s)cbp-spmenu-push-toleft(?!\S)/g , '' );
		} else {
			settings.body.className += " cbp-spmenu-push-toleft"
		}

		if (settings.animateStickNav) {
			if ( window.pageYOffset >= globals.nav_stick_on ) {
				$('div.main-nav').toggleClass('cbp-spnav-push-toleft');
			};
		};

		if (typeof callback == 'function') {
			callback.call(this, settings);
		};

		return false;
	}
})(document, jQuery);

(function() {

	'use strict';
	app.init();

	$(window).scroll(function(event) {
		$('div.main-nav').stickNav({top: globals.nav_stick_on}, function(settings){
			if (this.hasClass('cbp-spnav-push-toleft') && settings.isSticked == false) {
				this.removeClass('cbp-spnav-push-toleft');
			} else if ($('#cbp-spmenu-s2').hasClass('cbp-spmenu-open') && settings.isSticked == true) {
				this.addClass('cbp-spnav-push-toleft');
			};

			var top = parseInt($(this).height());
			$('div.container-dropdown').stickNav({top: globals.nav_stick_on, stickTo: top})
		});
	});

})();