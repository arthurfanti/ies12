<?php
	$args        = array( 'post_type' => 'redes' );
	$posts_array = get_posts( $args );
	
	get_header();
 ?>
		<div id="content" role="main">
			<section role="page" content="redes">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="small-10 small-centered medium-12 columns text-columns text-justify">
						<h2 class="blue uppercase">sobre nossas redes</h2>
						<p>You guys aren't Santa! You're not even robots. How dare you lie in front of Jesus? You, a bobsleder!? That I'd like to see! You can't just have your characters announce how they feel. That makes me feel angry! We can't compete with Mom! Why, those are the Grunka-Lunkas! They work here in the Slurm factory. If rubbin' frozen dirt in your crotch is wrong, hey I don't wanna be right. Come, Comrade Bender! We must take to the streets! Bender, being God isn't easy.</p>
						<p class="lead blue">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus amet eaque officiis fugiat vel, ab nisi, voluptatibus expedita aperiam quo. Impedit perspiciatis quaerat, vero nesciunt id porro commodi inventore reprehenderit.</p>
						<p>You guys aren't Santa! You're not even robots. How dare you lie in front of Jesus? You, a bobsleder!? That I'd like to see! You can't just have your characters announce how they feel. That makes me feel angry! We can't compete with Mom! Why, those are the Grunka-Lunkas! They work here in the Slurm factory. If rubbin' frozen dirt in your crotch is wrong, hey I don't wanna be right. Come, Comrade Bender! We must take to the streets! Bender, being God isn't easy. If you do too much, people get dependent on you, and if you do nothing, they lose hope. You have to use a light touch. Like a safecracker, or a pickpocket. Good news, everyone! There's a report on TV with some very bad news! I'm a thing. Hey, what kinda party is this? There's no booze and only one hooker.</p>
						<p>For example, if you killed your grandfather, you'd cease to exist! All I want is to be a monkey of moderate intelligence who wears a suit… that's why I'm transferring to business school! Of all the friends I've had… you're the first. That's a popular name today. Little "e", big "B"? Well I'da done better, but it's plum hard pleading a case while awaiting trial for that there incompetence. If rubbin' frozen dirt in your crotch is wrong, hey I don't wanna be right. Yes, if you make it look like an electrical fire. When you do things right, people won't be sure you've done anything at all. Hey, what kinda party is this? There's no booze and only one hooker. You guys aren't Santa! You're not even robots. How dare you lie in front of Jesus? Come, Comrade Bender! We must take to the streets! You lived before you met me?! Um, is this the boring, peaceful kind of taking to the streets? Kids have names? Doomsday device?</p>
					</div>
				</div>
				
				<article>
					<div class="row collapse">
						<div class="small-10 small-centered medium-12 columns">
							<h2 class="blue uppercase">conheça nossas redes</h2>
							<ul class="small-block-grid-1 medium-block-grid-3">
								<li>
									<img data-src="holder.js/340x340/gray/text: Escolha ao lado uma rede com a qual mais se identifique e seja bem-vindo! :)" alt="">
								</li>
								<?php foreach ($posts_array as $rede): ?>
								<li>
									<a href="<?= get_permalink($rede->ID) ?>">
										<?= get_the_post_thumbnail( $rede->ID, 'large' ); ?>
										<div class="text-center">
											<span>
												<cite>
													<?= apply_filters( 'the_title', 'Rede de ' . $rede->post_title, $rede->ID ); ?>
													<small>veja mais</small>
												</cite>
											</span>
										</div>
									</a>
								</li>
								<?php endforeach ?>
							</ul>

						</div>
					</div>
				</article>
			</section>
		</div>
		
		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>