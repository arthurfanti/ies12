<?php get_header(); ?>
		<div id="content" role="main">
			<section role="page" content="redes">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="small-10 small-centered medium-12 columns text-columns text-justify">
						<?php $content = the_content_vars( 'more' ); print $content[0]; ?>
					</div>
				</div>

				<article class="unique block">
					<div class="row">
						<h2 class="text-center">Como vamos fazer isso? Através dos passos da Visão Celular M12.</h2>
						<div class="small-6 medium-3 columns text-center">
							<img src="<?= get_template_directory_uri(); ?>/images/m12-ganhar.png" alt="">
							<h3 class="uppercase">Ganhar</h3>
							<p>Quando Deus criou o mundo e o homem, o fez guiado por Sua incomparável e inimitável capacidade criativa.</p>
						</div>

						<div class="small-6 medium-3 columns text-center">
							<img src="<?= get_template_directory_uri(); ?>/images/m12-consolidar.png" alt="">
							<h3 class="uppercase">Consolidar</h3>
							<p>Quando Deus criou o mundo e o homem, o fez guiado por Sua incomparável e inimitável capacidade criativa.</p>
						</div>

						<div class="small-6 medium-3 columns text-center">
							<img src="<?= get_template_directory_uri(); ?>/images/m12-discipular.png" alt="">
							<h3 class="uppercase">Discipular</h3>
							<p>Quando Deus criou o mundo e o homem, o fez guiado por Sua incomparável e inimitável capacidade criativa.</p>
						</div>

						<div class="small-6 medium-3 columns text-center">
							<img src="<?= get_template_directory_uri(); ?>/images/m12-enviar.png" alt="">
							<h3 class="uppercase">Enviar</h3>
							<p>Quando Deus criou o mundo e o homem, o fez guiado por Sua incomparável e inimitável capacidade criativa.</p>
						</div>
					</div>
				</article>

				<div class="spacing"></div>

				<div class="row">
					<div class="small-10 small-centered medium-12 columns text-columns text-justify">
						<?php print $content[1]; ?>
					</div>
				</div>
			</section>
		</div>
		
		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>