		<footer>
			<div class="wrapper">
				<div class="row">
					<div class="sitemap-col small-3 small-centered medium-uncentered columns">
						<a class="uppercase" href="<?php home_url( '/' ); ?>">Home</a>
						<a class="uppercase">Igreja El Shaddai</a>
						<ul class="hide-for-small-only">
							<li><a href="<?= get_page_link( get_page_by_title( 'historia' ) ); ?>">História</a></li>
							<li>
								<a>Unidades</a>
								<ul>
									<li><a href="<?php printf(get_post_type_archive_link( 'igrejas' ) . '%s', 'sede') ?>">Sede</a></li>
									<li><a href="<?= get_post_type_archive_link( 'igrejas' ); ?>?tipo=elshaddai">Igrejas El Shaddai</a></li>
									<li><a href="<?= get_post_type_archive_link( 'igrejas' ); ?>?tipo=cobertura">Igrejas de Aliança</a></li>
								</ul>
							</li>
							<li><a href="<?= get_page_link( get_page_by_title( 'Visão Celular M12' ) ); ?>">Visão celular no Modelo dos 12</a></li>
						</ul>
						<a href="<?= get_post_type_archive_link( 'lideranca' ); ?>" class="uppercase">Liderança</a>
						<ul>
							<li><a href="<?php printf(get_post_type_archive_link( 'lideranca' ) . '%s', 'apostolo-fabio-abbud') ?>">Apóstolo Fábio Abbud</a></li>
							<li><a href="<?php printf(get_post_type_archive_link( 'lideranca' ) . '%s', 'apostola-claudia-abbud') ?>">Apóstola Cláudia Abbud</a></li>
							<li><a href="<?= get_post_type_archive_link( 'lideranca' ); ?>">Pastores</a></li>
						</ul>
					</div>
					<div class="sitemap-col small-3 small-centered medium-uncentered columns">

						<a class="uppercase" href="<?= get_post_type_archive_link( 'ministerio' ); ?>">Ministérios</a>
						<ul class="hidden-for-small-only">
						<?php
							$ministerios_array = get_posts( array('post_type' => 'ministerio') );
							foreach ($ministerios_array as $ministerio): ?>
							<li><a href="<?= get_permalink( $ministerio->ID ); ?>"><?= get_the_title( $ministerio->ID ); ?></a></li>
						<?php endforeach ?>
						</ul>
						
						<a class="uppercase" href="http://celulas.ies12.com">Células</a>
						<ul class="hidden-for-small-only">
							<li><a href="http://celulas.ies12.com">Buscador de Células</a></li>
						</ul>
					</div>
					<div class="sitemap-col small-3 small-centered medium-uncentered columns">
						<a class="uppercase" href="<?= get_post_type_archive_link( 'redes' ); ?>">Redes</a>
						<ul class="hidden-for-small-only">
						<?php
							$redes_array = get_posts( array('post_type' => 'redes') );
							foreach ($redes_array as $rede): ?>
							<li><a href="<?= get_permalink( $rede->ID ); ?>"><?= get_the_title( $rede->ID ); ?></a></li>
						<?php endforeach ?>
						</ul>

						<a href="<?= get_post_type_archive_link( 'noticias' ); ?>" class="uppercase">Notícias</a>
						<ul class="hidden-for-small-only">
							<?php $related_query = new WP_Query(array('post_type' => 'noticias', 'posts_per_page' => 5, 'orderby' => 'id')); ?>
							<?php if ( $related_query->have_posts() ) : while ( $related_query->have_posts() ) : $related_query->the_post(); ?>
							<!-- post -->
							<li>
								<a href="<?= the_permalink(); ?>"><?= the_title(); ?></a>
							</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<!-- post navigation -->
							<?php else: ?>
							<h4>:(</h4>
							<?php endif; ?>
						</ul>
					</div>
					<div class="sitemap-col small-3 small-centered medium-uncentered columns">
						<a class="uppercase">Mídias</a>
						<ul class="hidden-for-small-only">
							<li><a href="//www.flickr.com/photos/ies12/sets/">Fotos</a></li>
							<li><a href="//www.youtube.com/ies12com">Vídeos</a></li>
							<li><a href="//soundcloud.com/ies12">Aúdios</a></li>
						</ul>

						<a href="#" class="uppercase">TV El Shaddai</a>
						<ul class="hidden-for-small-only">
							<li><a href="#">Programa IES12 - A Resposta</a></li>
						</ul>

						<a class="uppercase">Estudo</a>
						<ul class="hidden-for-small-only">
							<li><a href="<?= get_post_type_archive_link('estudoscelula'); ?>">Palavra de Célula</a></li>
							<li><a href="<?= get_post_type_archive_link('estudosdiscipulado'); ?>">Palavra de Discipulado</a></li>
						</ul>

						<a href="<?= get_post_type_archive_link('eventos'); ?>" class="uppercase">Agenda</a>
						<a href="<?= get_permalink( get_page_by_title('contato') ); ?>" class="uppercase">Contato</a>
					</div>
				</div>
			</div>

			<div class="bottombar" role="contactinfo">
				<div class="wrapper">
					<div class="row">
						<div class="small-8 small-centered medium-uncentered columns">
							<div class="row">
								<div class="small-3 columns">
									<h1 class="logo pb">Igreja El Shaddai</h1>
								</div>
								<div class="small-9 medium-8 large-6 columns end">
									<span class="contact">
										Igreja El Shaddai Comunidade Cristã
										<br>Rua Mont’Alverne, 363, Ipiranga, São Paulo/SP 
										<br>+55 11 2068 4020 | 2068 9285
										<br>contato@ies12.com
									</span>
								</div>
							</div>
						</div>
						<div class="small-7 small-centered medium-4 medium-uncentered columns">
							<div role="social-media">
								<ul class="inline-list radius">
									<li><a href="#" class="social icon-youtube"></a></li>
									<li><a href="#" class="social icon-flicker"></a></li>
									<li><a href="#" class="social icon-googleplus-01"></a></li>
									<li><a href="#" class="social icon-facebook"></a></li>
									<li><a href="#" class="social icon-twitter"></a></li>
									<li><a href="#" class="social icon-instagram"></a></li>
								</ul>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="small-5 medium-4 small-centered columns">
							<span>
								Desenvolvido por
								<h2 class="logo-domini">Domini Design</h2>
							</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<?php if (is_singular('noticias')): ?>
			<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				var disqus_shortname = 'ies12'; // required: replace example with your forum shortname
				
				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function () {
					var s = document.createElement('script'); s.async = true;
					s.type = 'text/javascript';
					s.src = '//' + disqus_shortname + '.disqus.com/count.js';
					(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
				}());
			</script>
		<?php endif ?>
		<?php wp_footer(); ?>



	</body>
</html>