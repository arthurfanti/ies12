<?php get_header() ?>
		<?php if( have_rows('banners') ): ?>
		<ul class="home-slider" data-orbit data-options="navigation_arrows:false; slide_number:false; timer:false;">
		<?php while( have_rows('banners') ): the_row(); 
			// vars
			$image   = get_sub_field('rp_imagem');
			$content = get_sub_field('rp_titulo');
			$link    = get_sub_field('rp_url');
			?>
			<li class="slide">
			<?php if( $link ): ?>
				<a href="<?php echo $link; ?>">
			<?php endif; ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
			<?php if( $link ): ?>
				</a>
			<?php endif; ?>
			</li>
		<?php endwhile; ?>
		</ul>
	<?php endif; ?>

		<div id="content" role="main">
			<section role="noticias">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<h2 class="icon-noticias blue uppercase">
							notícias
							<small>canal de notícias da igreja el shaddai</small>
						</h2>
					</div>
				</div>
				<div class="wrapper" style="padding:0 1%">
					<div id="newsSliderContainer" class="row collapse">
						<div class="small-12 medium-8 columns">
							<ul id="newsSlider" class="news-slider" data-orbit data-options="slide_number:false; bullets:false; timer:false;">
							<?php $hlNews_query = new WP_Query(array('post_type' => 'noticias', 'posts_per_page' => 5, 'orderby' => 'modified', 'order' => 'DESC', 'meta_query' => array( array('key' => 'destaque', 'value' => '1', 'compare' => '==') ))); ?>
							<?php if( $hlNews_query->have_posts() ) : while( $hlNews_query->have_posts() ) : $hlNews_query->the_post(); ?>
								<!-- post -->
								<li>
									<?php if( has_post_thumbnail() ) the_post_thumbnail('thumb-highlight'); ?>
									<!-- <img src="<?= get_template_directory_uri(); ?>/images/slider/jerusalem.jpg" alt="Congresso de Porto Seguro" /> -->
								</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
								<!-- post navigation -->
							<?php else: ?>
								<h4>:(</h4>
							<?php endif; ?>
							</ul>
						</div>
					
						<div class="small-12 medium-4 columns captions">
							<ul>
							<?php if( $hlNews_query->have_posts() ) : while( $hlNews_query->have_posts() ) : $hlNews_query->the_post(); ?>
								<!-- post -->
								<li><em><?php the_title(); ?></em></li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<?php else: ?>
								<h4>:(</h4>
							<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</section>

			<section role="categories">
				<div class="row">
					<div class="small-10 small-centered medium-4 medium-uncentered columns">
						<h2 class="blue uppercase">Eventos</h2>
						<ul>
							<?php $eventNews_query = new WP_Query(array('post_type' => 'noticias', 'news-category' => 'eventos', 'posts_per_page' => 3, 'orderby' => 'id', 'order' => 'DESC')); ?>
							<?php if ( $eventNews_query->have_posts() ) : while ( $eventNews_query->have_posts() ) : $eventNews_query->the_post(); ?>
							<!-- post -->
							<li><a class="text-justify" href="<?= the_permalink(); ?>"><?= ( strlen(get_the_title()) > 39 ) ? substr(get_the_title(), 0, 39) . "..." : get_the_title(); ?></a></li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<!-- post navigation -->
							<?php else: ?>
							<h4>:(</h4>
							<?php endif; ?>
						</ul>
						<a href="#" class="button tiny blue block">ver mais</a>
					</div>
					<div class="small-10 small-centered medium-4 medium-uncentered columns">
						<h2 class="blue uppercase">Por Dentro</h2>
						<ul>
							<?php $insiderNews_query = new WP_Query(array('post_type' => 'noticias', 'news-category' => 'por-dentro', 'posts_per_page' => 3, 'orderby' => 'id', 'order' => 'DESC')); ?>
							<?php if ( $insiderNews_query->have_posts() ) : while ( $insiderNews_query->have_posts() ) : $insiderNews_query->the_post(); ?>
							<!-- post -->
							<li><a class="text-justify" href="<?= the_permalink(); ?>"><?= ( strlen(get_the_title()) > 39 ) ? substr(get_the_title(), 0, 39) . "..." : get_the_title(); ?></a></li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<!-- post navigation -->
							<?php else: ?>
							<h4>:(</h4>
							<?php endif; ?>
						</ul>
						<a href="#" class="button tiny blue block">ver mais</a>
					</div>
					<div class="small-10 small-centered medium-4 medium-uncentered columns">
						<h2 class="blue uppercase">Estudos</h2>
						<ul>
							<?php $studies_query = new WP_Query(array('post_type' => array('estudoscelula', 'estudosdiscipulado'), 'posts_per_page' => 3, 'orderby' => 'id', 'order' => 'DESC')); ?>
							<?php if ( $studies_query->have_posts() ) : while ( $studies_query->have_posts() ) : $studies_query->the_post(); ?>
							<!-- post -->
							<li><a class="text-justify" href="<?= the_permalink(); ?>"><?= ( strlen(get_the_title()) > 39 ) ? substr(get_the_title(), 0, 39) . "..." : get_the_title(); ?></a></li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
							<!-- post navigation -->
							<?php else: ?>
							<h4>:(</h4>
							<?php endif; ?>
						</ul>
						<a href="#" class="button tiny blue block">ver mais</a>
					</div>
				</div>
			</section>

			<section role="videos" style="background:
											radial-gradient(#cf3237 13%, transparent 14%) 0px 0,
											radial-gradient(#cf3237 13%, transparent 14%) 7px 7px,
											radial-gradient(rgba(255,255,255,.1) 13%, transparent 20%) 0 1px,
											radial-gradient(rgba(255,255,255,.1) 13%, transparent 20%) 7px 8px;
											background-color:#e4373d;
											background-size:14px 14px;">
				<div class="row">
					<div class="small-12 columns">
						<h2 class="icon-video uppercase">
							vídeos
							<small>assista aos vídeos dos últimos cultos</small>
						</h2>
						<div class="row">
							<div class="small-12 medium-3 columns">
								<iframe width="100%" height="142" src="//www.youtube-nocookie.com/embed/iB62CLEWWMU?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
								<span>
									26/10/2014 <br>
									<cite>Culto de Domingo - Apóstolo Fábio Abbud</cite>
								</span>
							</div>
							<div class="small-12 medium-3 columns">
								<iframe width="100%" height="142" src="//www.youtube-nocookie.com/embed/gDUQ7iYc5E8?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
								<span>
									19/10/2014 <br>
									<cite>Culto de Domingo - Pr Carlos Henrique</cite>
								</span>
							</div>
							<div class="small-12 medium-3 columns">
								<iframe width="100%" height="142" src="//www.youtube-nocookie.com/embed/utb83wrfe3Y?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
								<span>
									12/10/2014 <br>
									<cite>Culto de Domingo - Pr Fred Bauerfeldt</cite>
								</span>
							</div>
							<div class="small-12 medium-3 columns">
								<iframe width="100%" height="142" src="//www.youtube-nocookie.com/embed/Y7yLAbG9haA?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
								<span>
									05/10/2014 <br>
									<cite>Culto de Domingo - Apóstolo Fábio Abbud</cite>
								</span>
							</div>
						</div>
						<div class="row">
							<div class="blacky text-center" style="margin-top:1rem">
								<a href="#" class="button uppercase radius disabled">ver todos os vídeos</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section role="lideranca">
				<div class="row">
					<div class="small-10 small-centered medium-6 medium-uncentered columns">
						<h2 class="icon-perfil-pastor orange uppercase">
							Apóstolos
							<small>veja o perfil completo dos apóstolos</small>
						</h2>
						<ul class="small-block-grid-1 medium-block-grid-2">
							<li>
								<a href="<?php printf(get_post_type_archive_link( 'lideranca' ) . '%s', 'apostolo-fabio-abbud') ?>">
									<img src="<?= get_template_directory_uri(); ?>/images/pastores/apostolo-fabio-abbud.jpg" alt="">
									<strong>FÁBIO ABBUD</strong><br>
									<span>Apóstolo | Presidente</span>
								</a>
							</li>
							<li>
								<a href="<?php printf(get_post_type_archive_link( 'lideranca' ) . '%s', 'apostola-claudia-abbud') ?>">
									<img src="<?= get_template_directory_uri(); ?>/images/pastores/apostola-claudia-abbud.jpg" alt="">
									<strong>CLÁUDIA ABBUD</strong><br>
									<span>Apóstola | Presidente</span>
								</a>
							</li>
						</ul>
					</div>
					<div class="small-10 small-centered medium-6 medium-uncentered columns">
						<h2 class="icon-perfil-pastor orange uppercase">
							pastores
							<small>veja o perfil completo dos pastores</small>
						</h2>
						<div id="pastores-slide">
							<?php
								$args = array('post_type' => 'lideranca', 'leadership' => 'pastor');
								$pastor_query = new WP_Query( $args );

								if ( $pastor_query->have_posts() ) : while ( $pastor_query->have_posts() ) : $pastor_query->the_post(); ?>
								<!-- post -->
								<div>
									<a href="<?php the_permalink(); ?>">
										<?php if(has_post_thumbnail()) the_post_thumbnail('large-thumb'); ?>
										<strong class="uppercase"><?php the_title(); ?></strong>
										<p>Pastores | Equipe de 12</p>
									</a>
								</div>
								<?php endwhile; ?>
								<!-- post navigation -->
								<?php else: ?>
								<h4>Desculpe, não há pastores cadastrados no momento!</h4>
								<?php endif; ?>
						</div>
						<div class="row">
							<div class="orange text-center">
								<a href="<?= get_post_type_archive_link( 'lideranca' ); ?>" class="button uppercase radius">veja todos os pastores</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section role="tv">
				<div class="row">
					<div class="small-10 small-centered medium-7 medium-uncentered columns">
						<h2 class="icon-tvelshaddai uppercase">
							TV EL SHADDAI
							<!-- <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small> -->
						</h2>
						<iframe width="100%" height="315" src="//www.youtube-nocookie.com/embed/tzGMKI1uFYI?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</div>
					<div id="conheca-programa" class="small-10 small-centered medium-5 medium-uncentered columns">
						<a href="#">
							<img src="<?= bloginfo('template_directory') ?>/images/programa-resposta.png" alt="">
						</a>
					</div>
				</div>
			</section>

			<section role="medias">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<h2 class="icon-socialicon blue uppercase">
							mídias
							<small>Confira abaixo os nosso canais:</small>
						</h2>
					</div>
					
					<div class="small-10 small-centered medium-12 columns">
						<ul class="small-block-grid-1 medium-block-grid-3">
							<li>
								<a href="//www.youtube.com/ies12com">
									<img src="<?= get_template_directory_uri(); ?>/images/midia_videos.jpg" alt="">
									<div class="block red">
										<i class="icon-video"></i>
										<span class="uppercase">veja mais</span>
									</div>
								</a>
							</li>
							<li>
								<a href="//soundcloud.com/ies12">
									<img src="<?= get_template_directory_uri(); ?>/images/midia_audios.jpg" alt="">
									<div class="block yellow">
										<i class="icon-audio"></i>
										<span class="uppercase">veja mais</span>
									</div>
								</a>
							</li>
							<li>
								<a href="//www.flickr.com/photos/ies12/sets/">
									<img src="<?= get_template_directory_uri(); ?>/images/midia_fotos.jpg" alt="">
									<div class="block pinky">
										<i class="icon-foto"></i>
										<span class="uppercase">veja mais</span>
									</div>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</section>

			<section role="quote">
				<div class="row">
					<div class="small-12 column">
						<h2>
							2014
							<small>ampliando a colheita pelo princípio da honra</small>
						</h2>
					</div>
					<div class="small-12 column">
						<blockquote class="text-center">Honra teu pai e tua mãe, como te ordenou o Senhor, o teu Deus, para que tenhas longa vida e tudo te vá bem na terra que o Senhor, o teu Deus, te dá.</blockquote>
						<strong class="text-center">Deuteronômio 5:16</strong>
					</div>
				</div>
			</section>

			<section role="agenda">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<h2 class="icon-agenda purple uppercase">
							Agenda
							<small>acompanhe todos os eventos da igreja</small>
						</h2>
					</div>
					<div class="small-10 small-centered medium-5 medium-uncentered columns">
						<ul class="agenda">
							<?php
								$args1 = array( 'post_type' => 'eventos', 'posts_per_page' => 3, 'order' => 'ASC' );
								$args2 = array( 'post_type' => 'eventos', 'posts_per_page' => 1, 'meta_query' => array(array('key' => 'sticky_event', 'value' => '1', 'compare' => '==')) );
								$events_query = new WP_Query( $args1 );
								$sticky_event = new WP_Query( $args2 );

								if ( $events_query->have_posts() ) : while ( $events_query->have_posts() ) : $events_query->the_post(); ?>
								<!-- post -->
								<?php
									$date = get_field('data');
									
									// extract Y,M,D
									$y    = substr($date, 0, 4);
									$m    = substr($date, 4, 2);
									$d    = substr($date, 6, 2);
									
									// create UNIX
									$time = strtotime("{$d}-{$m}-{$y}");
									$mes  = strftime('%b', $time);
								?>
								<li>
									<div class="data strong">
										<strong><?= $d ?></strong><?= $mes ?>
									</div>
									<span>
										<strong><?php the_title(); ?></strong>
										<small><?= get_field('local') ?></small>
									</span>
								</li>
								<?php endwhile; ?>
								<!-- post navigation -->
								<?php else: ?>
								<!-- no posts found -->
								<?php endif; ?>
							<li>
								<div class="purple text-center">
									<a href="<?= get_post_type_archive_link( 'eventos' ); ?>" class="button radius uppercase">ver todos os eventos</a>
								</div>
							</li>
						</ul>
					</div>
					<div class="small-10 small-centered medium-7 medium-uncentered columns">
							<?php if ( $sticky_event->have_posts() ) : while ( $sticky_event->have_posts() ) : $sticky_event->the_post(); ?>
							<!-- post -->
							<?php
								$image = get_field('sticky_event_image');
								$date  = get_field('data');
								
								// extract Y,M,D
								$y     = substr($date, 0, 4);
								$m     = substr($date, 4, 2);
								$d     = substr($date, 6, 2);
								
								// create UNIX
								$time  = strtotime("{$d}-{$m}-{$y}");
								$mes   = strftime('%b', $time);
							?>
						<div style="
									height:345px; 
									overflow:hidden; 
									background-image: url(<?= $image['url'] ?>); 
									background-size:cover; 
									background-position:50% 50%"  class="evento-destaque">
							<div style="margin-top:255px" class="row collapse">
								<div class="small-2 columns yellow block">
									<span><strong><?= $d ?></strong><?= $mes ?></span>
								</div>
								<div class="small-10 columns purple block">
									<span><strong><?php the_title(); ?></strong><?= get_field('local'); ?></span>
								</div>
							</div>
						</div>
							<?php endwhile; ?>
							<!-- post navigation -->
							<?php else: ?>
							<!-- no posts found -->
							<?php endif; ?>
					</div>
				</div>
			</section>
		</div>
<?php get_footer() ?>