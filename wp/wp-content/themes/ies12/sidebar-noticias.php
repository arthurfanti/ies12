<?php
	$tax_list = get_post_taxonomies($post->ID);

	foreach ($tax_list as $tax) {
		if ( preg_match('/("cell" || "disciple")-studies-category$/', $tax) ) {
			$tax = $tax;
		}
	}

	$terms = get_terms( $tax, array('orderby' => 'id', 'order' => 'ASC',  'hide_empty' => false) );
?>
						<aside role="sidebar" class="<?= ( get_post_type() != 'post') ? get_post_type() : '' ?>">
							<form id="filter-search">
								<div class="row collapse">
									<div class="small-9 large-10 columns">
										<input type="text" placeholder="O que você está procurando?">
									</div>
									<div class="small-3 large-2 columns">
										<a href="#" class="icon-pesquisa button postfix"></a>
									</div>
								</div>
							</form>

							<div class="panel custom-panel">
								<strong class="uppercase">mais lidas</strong>
								<ol>
									<?php $related_query = new WP_Query(array('post_type' => get_post_type(), 'posts_per_page' => 5, 'orderby' => 'meta_value_num', 'meta_key' => 'post_views_count')); ?>
									<?php if ( $related_query->have_posts() ) : while ( $related_query->have_posts() ) : $related_query->the_post(); ?>
									<!-- post -->
									<li><a href="<?= the_permalink(); ?>"><?= the_title(); ?></a></li>
									<?php endwhile; ?>
									<?php wp_reset_postdata(); ?>
									<!-- post navigation -->
									<?php else: ?>
									<span>Sorry, nothing to see here! :(</span>
									<?php endif; ?>
								</ol>
							</div>

							<div class="panel custom-panel">
								<strong class="uppercase">categorias</strong>
								<ul>
									<?php
										foreach ($terms as $term) {
											$term_link = get_term_link( $term );
											if (is_wp_error( $term )) {
												continue;
											}
											echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
										}
									?>
								</ul>
							</div>

							<?php if (get_post_type() == 'noticias'): ?>	
							<div class="panel">
								<strong class="uppercase">quem escreve</strong>
								<ul class="profile-grid small-block-grid-1 medium-block-grid-2">
									<li>
										<a title="Bruce Waine" href="#">
											<img data-src="holder.js/140x140/industrial" alt="">
										</a>
									</li>
									<li>
										<a title="Peter Quill" href="#">
											<img data-src="holder.js/140x140/social" alt="">
										</a>
									</li>
									<li>
										<a title="Sheldon Cooper" href="#">
											<img data-src="holder.js/140x140/sky" alt="">
										</a>
									</li>
									<li>
										<a title="Clara Oswald" href="#">
											<img data-src="holder.js/140x140/gray" alt="">
										</a>
									</li>
								</ul>
							</div>
							<?php endif ?>
						</aside>