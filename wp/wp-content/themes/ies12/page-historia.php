<?php
	$feedObject = new feedObject();
	$feedArray  = json_decode($feedObject->get_twitter_feed( 'IES12' ));
	
	get_header();
?>
		<div id="content" role="main">
			<section role="page" content="historia">
				<?php get_template_part( 'partials/content', 'page-header' ); ?>
				
				<div class="row collapse">
					<iframe width="100%" height="380" src="//www.youtube-nocookie.com/embed/3n0ppd5Nhfw?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>

				<div class="row collapse">
					<div class="small-10 small-centered medium-12 columns">
						<?= $post->post_content; ?>
					</div>
				</div>

				<div class="row collapse">
					<div class="small-12 columns">
						<hr>
						<strong class="uppercase">compartilhe</strong>
						<?php echo do_shortcode('[ssba]'); ?>
					</div>
				</div>
			</section>

			<section>
				<div class="row">
					<ul data-orbit data-options="slide_number:false; navigation_arrows:false; timer:false;">
						<?php foreach ($feedArray as $feed): ?>
							<li><?= $feed->text; ?></li>
						<?php endforeach ?>
					</ul>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer(); ?>