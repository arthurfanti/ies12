<?php

function theme_scripts() {
	$hash = hash('adler32', uniqid(rand(), true));

	wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr.min.js', array() );
	wp_enqueue_script('libraries', get_template_directory_uri() . '/js/vendor/libraries.min.js', array('jquery'), null, true );
	wp_enqueue_script('foundation', get_template_directory_uri() . '/js/vendor/foundation.min.js', array('jquery'), null, true );
	wp_enqueue_script('app', get_template_directory_uri() . '/js/app.min.js', null, $hash, true );
}

// register a custom post type called 'ministerios'
function wptp_create_post_type() {
	$labels = array( 
		'name'               => __( 'Ministerios' ),
		'singular_name'      => __( 'ministerio' ),
		'add_new'            => __( 'Criar ministerio' ),
		'add_new_item'       => __( 'Add New ministerio' ),
		'edit_item'          => __( 'Edit ministerio' ),
		'new_item'           => __( 'New ministerio' ),
		'view_item'          => __( 'View ministerio' ),
		'search_items'       => __( 'Search ministerios' ),
		'not_found'          =>  __( 'No ministerios Found' ),
		'not_found_in_trash' => __( 'No ministerios found in Trash' ),
	);
	$args = array(
		'labels'       => $labels,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => false,
		'menu_icon'    => 'dashicons-groups',
		'rewrite'      => array( 'slug' => 'ministerios', 'with-front' => true ),
		'supports'     => array(
			'title', 
			'editor', 
			'excerpt', 
			'custom-fields', 
			'thumbnail',
			'page-attributes'
		),
		// 'taxonomies' => array( 'post_tag', 'category'), 
	);

	$labels_noticias = array( 
		'name'               => __( 'Noticias' ),
		'singular_name'      => __( 'noticia' ),
		'add_new'            => __( 'Criar noticia' ),
		'add_new_item'       => __( 'Add New noticia' ),
		'edit_item'          => __( 'Edit noticia' ),
		'new_item'           => __( 'New noticia' ),
		'view_item'          => __( 'View noticia' ),
		'search_items'       => __( 'Search Noticias' ),
		'not_found'          =>  __( 'No Noticias Found' ),
		'not_found_in_trash' => __( 'No noticias found in Trash' ),
	);
	$args_noticias = array(
		'labels'       => $labels_noticias,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-format-quote',
		'taxonomies'   => array( 'post_tag' ), 
		'supports'     => array(
			'title',
			'editor',
			'excerpt',
			'comments',
			'thumbnail',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	$labels_estudosCelula = array( 
		'name'               => __( 'Estudos de Célula' ),
		'singular_name'      => __( 'Estudo de Célula' ),
		'add_new'            => __( 'Criar Estudo' ),
		'add_new_item'       => __( 'Criar novo Estudo' ),
		'edit_item'          => __( 'Editar Estudo' ),
		'new_item'           => __( 'Novo Estudo' ),
		'view_item'          => __( 'Ver Estudo' ),
		'search_items'       => __( 'Buscar Estudo' ),
		'not_found'          =>  __( 'Nenhum Estudo encontradp' ),
		'not_found_in_trash' => __( 'Nenhum Estudo na lixeira' ),
	);
	$args_estudosCelula = array(
		'labels'       => $labels_estudosCelula,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-book',
		'taxonomies'   => array( 'post_tag'),
		'rewrite'      => array( 'slug' => 'estudos-de-celula', 'with-front' => true ),
		'supports'     => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	$labels_estudosDiscipulado = array( 
		'name'               => __( 'Estudos de Discipulado' ),
		'singular_name'      => __( 'Estudo de Discipulado' ),
		'add_new'            => __( 'Criar Estudo' ),
		'add_new_item'       => __( 'Criar novo Estudo' ),
		'edit_item'          => __( 'Editar Estudo' ),
		'new_item'           => __( 'Novo Estudo' ),
		'view_item'          => __( 'Ver Estudo' ),
		'search_items'       => __( 'Buscar Estudo' ),
		'not_found'          =>  __( 'Nenhum Estudo encontrado' ),
		'not_found_in_trash' => __( 'Nenhum Estudo na lixeira' ),
	);
	$args_estudosDiscipulado = array(
		'labels'       => $labels_estudosDiscipulado,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-book',
		'taxonomies'   => array( 'post_tag'),
		'rewrite'      => array( 'slug' => 'estudos-de-discipulado', 'with-front' => true ),
		'supports'     => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	$labels_redes = array( 
		'name'               => __( 'Redes' ),
		'singular_name'      => __( 'Rede' ),
		'add_new'            => __( 'Criar Rede' ),
		'add_new_item'       => __( 'Criar novo Rede' ),
		'edit_item'          => __( 'Editar Rede' ),
		'new_item'           => __( 'Nova Rede' ),
		'view_item'          => __( 'Ver Rede' ),
		'search_items'       => __( 'Buscar Rede' ),
		'not_found'          =>  __( 'Nenhuma Rede encontrado' ),
		'not_found_in_trash' => __( 'Nenhuma Rede na lixeira' ),
	);
	$args_redes = array(
		'labels'       => $labels_redes,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-networking',
		'supports'     => array(
			'title',
			'editor',
			// 'excerpt',
			'thumbnail',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	$labels_lideranca = array( 
		'name'               => __( 'Liderança' ),
		'singular_name'      => __( 'Liderança' ),
		'add_new'            => __( 'Criar Líder' ),
		'add_new_item'       => __( 'Criar novo Líder' ),
		'edit_item'          => __( 'Editar Líder' ),
		'new_item'           => __( 'Novo Líder' ),
		'view_item'          => __( 'Ver Líder' ),
		'search_items'       => __( 'Buscar Líder' ),
		'not_found'          =>  __( 'Nenhum Líder encontrado' ),
		'not_found_in_trash' => __( 'Nenhum Líder na lixeira' ),
	);
	$args_lideranca = array(
		'labels'       => $labels_lideranca,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => true,
		'menu_icon'    => 'dashicons-awards',
		'supports'     => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'post-formats',
			// 'custom-fields',
			// 'page-attributes'
		),
	);

	$labels_igrejas = array( 
		'name'               => __( 'Igrejas' ),
		'singular_name'      => __( 'Igrejas' ),
		'add_new'            => __( 'Criar Igreja' ),
		'add_new_item'       => __( 'Criar novo Igreja' ),
		'edit_item'          => __( 'Editar Igreja' ),
		'new_item'           => __( 'Novo Igreja' ),
		'view_item'          => __( 'Ver Igreja' ),
		'search_items'       => __( 'Buscar Igreja' ),
		'not_found'          =>  __( 'Nenhum Igreja encontrado' ),
		'not_found_in_trash' => __( 'Nenhum Igreja na lixeira' ),
	);
	$args_igrejas = array(
		'labels'       => $labels_igrejas,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => false,
		'menu_icon'    => 'dashicons-location',
		'supports'     => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'post-formats',
		),
	);

	$labels_eventos = array( 
		'name'               => __( 'Eventos' ),
		'singular_name'      => __( 'Eventos' ),
		'add_new'            => __( 'Criar Evento' ),
		'add_new_item'       => __( 'Criar novo Evento' ),
		'edit_item'          => __( 'Editar Evento' ),
		'new_item'           => __( 'Novo Evento' ),
		'view_item'          => __( 'Ver Evento' ),
		'search_items'       => __( 'Buscar Evento' ),
		'not_found'          =>  __( 'Nenhum Evento encontrado' ),
		'not_found_in_trash' => __( 'Nenhum Evento na lixeira' ),
	);
	$args_eventos = array(
		'labels'       => $labels_eventos,
		'has_archive'  => true,
		'public'       => true,
		'hierarchical' => false,
		'menu_icon'    => 'dashicons-calendar',
		'rewrite'      => array( 'slug' => 'agenda', 'with-front' => false ),
		'supports'     => array(
			'title',
			// 'editor',
			// 'excerpt',
			// 'thumbnail',
			// 'post-formats',
		),
	);

	register_post_type( 'ministerio', $args );
	register_post_type( 'redes', $args_redes );
	register_post_type( 'Igrejas', $args_igrejas );
	register_post_type( 'Eventos', $args_eventos );
	register_post_type( 'noticias', $args_noticias );
	register_post_type( 'lideranca', $args_lideranca );
	register_post_type( 'estudoscelula', $args_estudosCelula );
	register_post_type( 'estudosdiscipulado', $args_estudosDiscipulado );
} 

// register taxonomies
function wptp_register_taxonomies() {
	 
	// register_taxonomy( 'ensaio', array('ministerio', 'redes'),
	// 	array(
	// 		'labels' => array(
	// 			'name'              => 'Ensaios',
	// 			'singular_name'     => 'Ensaio',
	// 			'search_items'      => 'Buscar Ensaios',
	// 			'all_items'         => 'Todos Ensaios',
	// 			'edit_item'         => 'Editar Ensaio',
	// 			'update_item'       => 'Atualizar Ensaio',
	// 			'add_new_item'      => 'Incluir Ensaio',
	// 			'new_item_name'     => 'Novo nome de Ensaio',
	// 			'menu_name'         => 'Ensaio',
	// 		),
	// 		'hierarchical' => true,
	// 		'sort' => true,
	// 		'args' => array( 'orderby' => 'term_order' ),
	// 		'show_admin_column' => true
	// 	)
	// );

	$labels_newsCategory = array(
		'name'              => _x( 'Categorias de Noticia', 'taxonomy general name' ),
		'singular_name'     => _x( 'Categoria de Noticia', 'taxonomy singular name' ),
		'search_items'      => __( 'Buscar Categorias' ),
		'all_items'         => __( 'Todas Categorias' ),
		'parent_item'       => __( 'Categoria Pai' ),
		'parent_item_colon' => __( 'Categoria Pai:' ),
		'edit_item'         => __( 'Editar Categoria' ),
		'update_item'       => __( 'Atualizar Categoria' ),
		'add_new_item'      => __( 'Incluir Categoria' ),
		'new_item_name'     => __( 'Nova da nova categoria' ),
		'menu_name'         => __( 'Categorias' )
	);

	$args_newsCategory = array(
		'hierarchical'      => true,
		'labels'            => $labels_newsCategory,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'noticias/categoria' )
	);

	$labels_cellStudiesCategory = array(
		'name'              => _x( 'Categorias de Estudo', 'taxonomy general name' ),
		'singular_name'     => _x( 'Categoria de Estudo', 'taxonomy singular name' ),
		'search_items'      => __( 'Buscar Categorias' ),
		'all_items'         => __( 'Todas Categorias' ),
		'parent_item'       => __( 'Categoria Pai' ),
		'parent_item_colon' => __( 'Categoria Pai:' ),
		'edit_item'         => __( 'Editar Categoria' ),
		'update_item'       => __( 'Atualizar Categoria' ),
		'add_new_item'      => __( 'Incluir Categoria' ),
		'new_item_name'     => __( 'Nova da nova categoria' ),
		'menu_name'         => __( 'Categorias' )
	);

	$args_cellStudiesCategory = array(
		'hierarchical'      => true,
		'labels'            => $labels_cellStudiesCategory,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'estudos-de-celula/categoria', 'with-front' => true )
	);

	$labels_discipleStudiesCategory = array(
		'name'              => _x( 'Categorias de Estudo', 'taxonomy general name' ),
		'singular_name'     => _x( 'Categoria de Estudo', 'taxonomy singular name' ),
		'search_items'      => __( 'Buscar Categorias' ),
		'all_items'         => __( 'Todas Categorias' ),
		'parent_item'       => __( 'Categoria Pai' ),
		'parent_item_colon' => __( 'Categoria Pai:' ),
		'edit_item'         => __( 'Editar Categoria' ),
		'update_item'       => __( 'Atualizar Categoria' ),
		'add_new_item'      => __( 'Incluir Categoria' ),
		'new_item_name'     => __( 'Nova da nova categoria' ),
		'menu_name'         => __( 'Categorias' )
	);

	$args_discipleStudiesCategory = array(
		'hierarchical'      => true,
		'labels'            => $labels_discipleStudiesCategory,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'estudos-de-discipulado/categoria', 'with-front' => true )
	);
	
	$labels_leaderType = array(
		'name'					=> _x( 'Tipo de liderança', 'Taxonomy plural name', 'ies12' ),
		'singular_name'			=> _x( 'Tipo de liderança', 'Taxonomy singular name', 'ies12' ),
		'search_items'			=> __( 'Buscar por tipo de liderança', 'ies12' ),
		'all_items'				=> __( 'Todos tipos de liderança', 'ies12' ),
		'parent_item'			=> __( 'Tipo de liderança pai', 'ies12' ),
		'parent_item_colon'		=> __( 'Tipo de liderança pai:', 'ies12' ),
		'edit_item'				=> __( 'Editar tipo de liderança', 'ies12' ),
		'update_item'			=> __( 'Atualizar tipo de liderança', 'ies12' ),
		'add_new_item'			=> __( 'Incluir tipo de liderança', 'ies12' ),
		'new_item_name'			=> __( 'Novo tipo de liderança', 'ies12' ),
		'menu_name'				=> __( 'Tipo de liderança', 'ies12' ),
	);

	$args_leaderType = array(
		'hierarchical'      => true,
		'labels'            => $labels_leaderType,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		// 'show_tagcloud'     => false,
	);


	$labels_churchCategory = array(
		'name'              => _x( 'Categorias de Igreja', 'taxonomy general name' ),
		'singular_name'     => _x( 'Categoria de Igreja', 'taxonomy singular name' ),
		'search_items'      => __( 'Buscar Categorias' ),
		'all_items'         => __( 'Todas Categorias' ),
		'parent_item'       => __( 'Categoria Pai' ),
		'parent_item_colon' => __( 'Categoria Pai:' ),
		'edit_item'         => __( 'Editar Categoria' ),
		'update_item'       => __( 'Atualizar Categoria' ),
		'add_new_item'      => __( 'Incluir Categoria' ),
		'new_item_name'     => __( 'Nova da nova categoria' ),
		'menu_name'         => __( 'Categorias' )
	);

	$args_churchCategory = array(
		'hierarchical'      => true,
		'labels'            => $labels_churchCategory,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'igrejas/categoria' )
	);

	register_taxonomy( 'leadership', array( 'lideranca' ), $args_leaderType );
	register_taxonomy( 'news-category', array( 'noticias' ), $args_newsCategory );
	register_taxonomy( 'church-category', array( 'igrejas' ), $args_churchCategory );
	register_taxonomy( 'cell-studies-category', array( 'estudoscelula' ), $args_cellStudiesCategory );
	register_taxonomy( 'disciple-studies-category', array( 'estudosdiscipulado' ), $args_discipleStudiesCategory );
}

function add_custom_meta_boxes() {

	// Define the custom attachment for posts
	add_meta_box(
		'cover_picture_attachment',
		'Imagem de Capa',
		'cover_picture_attachment',
		'ministerio',
		'side',
		'high'
	);

	add_meta_box(
		'content_picture_attachment',
		'Imagem Destaque',
		'content_picture_attachment',
		'ministerio',
		'side',
		'high'
	);

	// add_meta_box(
	// 	'shepherd_plus_one',
	// 	'Plus One',
	// 	'display_shepherd_plus_one',
	// 	'lideranca',
	// 	'normal',
	// 	'default'
	// );

	add_meta_box(
		'social_medias_uris',
		'Redes Sociais',
		'display_social_media_fields',
		'lideranca',
		'side',
		'default'
	);

	// $screens = array('ministerio', 'redes' );
	// foreach ($screens as $screen) {
	// 	add_meta_box(
	// 		'date_essay',
	// 		'Data de Ensaio',
	// 		'display_essay_datepicker',
	// 		$screen,
	// 		'side',
	// 		'high'
	// 	);
	// }

} // end add_custom_meta_boxes

function display_social_media_fields( $leader_facebook = false, $leader_twitter = false, $leader_instagram = false ) {
	global $post;
	$post_id = $post->ID;

	$leader_facebook  = get_post_meta( $post_id, 'leader_facebook', true );
	$leader_twitter   = get_post_meta( $post_id, 'leader_twitter', true );
	$leader_instagram = get_post_meta( $post_id, 'leader_instagram', true );
	$leader_nonce     = wp_nonce_field(plugin_basename(__FILE__), 'social_medias_nonce');

	?>
	<form>
		<?= (has_term( 'pastor', 'leadership' )) ? get_field('nome') : $post->post_title; ?>
		<hr>
		<fieldset>
			<label>
				Facebook <br />
				<input value="<?= $leader_facebook ?>" name="leader_facebook" type="text" placeholder="Nome de usuário">
			</label>
		</fieldset>
		<fieldset>
			<label>
				Twitter <br />
				<input value="<?= $leader_twitter ?>" name="leader_twitter" type="text" placeholder="Nome de usuário">
			</label>
		</fieldset>
		<fieldset>
			<label>
				Instagram <br />
				<input value="<?= $leader_instagram ?>" name="leader_instagram" type="text" placeholder="Nome de usuário">
			</label>
		</fieldset>
	</form>
	<?php
}

function display_essay_datepicker( $essay_final = false, $essay_final_format = false ) {
	global $post;
	$post_id = $post->ID;

	$essay_final        = get_post_meta( $post_id, 'essay_final', true  );
	$essay_final_format = get_post_meta( $post_id, 'essay_final_format', true  );
	$days               = array('segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado', 'domingo');
	$periods            = array('am', 'pm');

	wp_enqueue_script( 'selectize', '//cdn.jsdelivr.net/jquery.selectric/1.6.5/jquery.selectric.min.js', true);
	wp_enqueue_style( 'selectize', get_template_directory_uri() . '/css/selectric.css', true);

	wp_nonce_field(plugin_basename(__FILE__), 'essay_final_nonce');

	echo "<div class='misc-pub-section curtime misc-pub-curtime'>";
	echo "<span id='timestamp'>";
	print_r($essay_final_format);
	echo "</span>";
	echo "</div>";

	$html = "<select id='essay_day' name='essay_day'>";
	foreach ($days as $day) {
		$html .= "<option value=" . $day . ">" . $day . "</option>";
	}
	$html .= "</select>";

	$html .= "<select id='essay_hour' name='essay_hour'>";
	for ($h= 1; $h <= 12; $h++) { 
		$html .= "<option value=" . $h . ">" . $h . "</option>";
	}
	$html .= "</select>";

	$html .= "<select id='essay_min' name='essay_min'>";
	for ($m= 00; $m <= 50; $m+=10) {
		if($m == 0) $m .= 0;
		$html .= "<option value=" . $m . ">" . $m . "</option>";
	}
	$html .= "</select>";

	$html .= "<select id='essay_period' name='essay_period'>";
	foreach ($periods as $period) {
		$html .= "<option value=" . $period . ">" . $period . "</option>";
	}
	$html .= "</select>";

	$html .= "<input type='hidden' name='essay_final' id='essay_final' value='" . $essay_final . "' />";

	$html .= "<script>\n";
	$html .="jQuery(document).ready(function($) {\n";
	$html .="	$('select[id*=\"essay_\"]').selectric().change(function(event) {\n";
	$html .="		var value = '';\n";
	$html .="		$('select[id*=\"essay_\"]').each(function(index, el) {\n";
	$html .="			value += $(this).val() + ',';\n";
	$html .="		});\n";
	$html .="		$('#essay_final').val(value);\n";
	$html .="	});\n";
	$html .="});\n";
	$html .="</script>";

	echo $html;
}

// function para criar metabox com date e time pickers
// function display_essay_datepicker( $cookie ) {
// 	// Enqueue Datepicker + jQuery UI CSS
// 	wp_enqueue_script( 'jquery-ui-datepicker' );
// 	wp_enqueue_script( 'jquery-ui-datepicker-addon', get_template_directory_uri() . '/js/vendor/jquery-ui-timepicker-addon.js', true);
// 	wp_enqueue_style( 'jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/smoothness/jquery-ui.css', true);
// 	wp_enqueue_style( 'jquery-ui-addon', get_template_directory_uri() . '/css/jquery-ui-timepicker-addon.css', true);

// 	// Retrieve current date for cookie
// 	$cookie_date = get_post_meta( $cookie->ID, 'cookie_date', true  );
	
// 	$html = "<script>";
// 	$html .="jQuery(document).ready(function(){";
// 	$html .="jQuery('#cookie_date').datetimepicker({";
// 	$html .="addSliderAccess: true,";
// 	$html .="sliderAccessArgs: { touchonly: false }";
// 	$html .="});";
// 	$html .="});";
// 	$html .="</script>";
// 	$html .="<input type='text' name='cookie_cookie_date' id='cookie_date' value='" . $cookie_date . "' />";

// 	echo $html;
// }

function cover_picture_attachment() {

	wp_nonce_field(plugin_basename(__FILE__), 'cover_picture_attachment_nonce');
	 
	$html = '<p class="description">';
	$html .= 'Selecionar Imagem.';
	$html .= '</p>';
	$html .= '<input type="file" id="cover_picture_attachment" name="cover_picture_attachment" value="" size="25" />';
	 
	echo $html;

} // end cover_picture_attachment

function content_picture_attachment() {

	wp_nonce_field(plugin_basename(__FILE__), 'content_picture_attachment_nonce');
	 
	$html = '<p class="description">';
	$html .= 'Selecionar Imagem.';
	$html .= '</p>';
	$html .= '<input type="file" id="content_picture_attachment" name="content_picture_attachment" value="" size="25" />';
	 
	echo $html;

} // end cover_picture_attachment

function save_custom_meta_data($ID = false, $post = false) {

	if ( isset($_POST['post_type']) && "lideranca" == $_POST['post_type'] ) {
		if(!wp_verify_nonce($_POST['social_medias_nonce'], plugin_basename(__FILE__))) {
			return $ID;
		}
			 
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $ID;
		}
			 
		if('page' == $_POST['post_type']) {
			if(!current_user_can('edit_page', $ID)) {
			return $ID;
			}
		} else {
			if(!current_user_can('edit_page', $ID)) {
				return $ID;
			}
		}

		if ( !empty($_POST['leader_facebook']) && !empty($_POST['leader_twitter']) && !empty($_POST['leader_instagram']) ) {
			update_post_meta($ID, 'leader_facebook', $_POST['leader_facebook']);
			update_post_meta($ID, 'leader_twitter', $_POST['leader_twitter']);
			update_post_meta($ID, 'leader_instagram', $_POST['leader_instagram']);
		}

	}

	if ( isset($_POST['post_type']) && 'ministerio' == $_POST['post_type'] ) {
		/* --- security verification --- */
		if(!wp_verify_nonce($_POST['cover_picture_attachment_nonce'], plugin_basename(__FILE__))) {
			return $ID;
		} // end if

		if(!wp_verify_nonce($_POST['content_picture_attachment_nonce'], plugin_basename(__FILE__))) {
			return $ID;
		} // end if

		// if(!wp_verify_nonce($_POST['essay_final_nonce'], plugin_basename(__FILE__))) {
		// 	return $ID;
		// } // end if
			 
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $ID;
		} // end if
			 
		if('page' == $_POST['post_type']) {
			if(!current_user_can('edit_page', $ID)) {
			return $ID;
			} // end if
		} else {
			if(!current_user_can('edit_page', $ID)) {
				return $ID;
			} // end if
		} // end if
		/* - end security verification - */
	} 

	// elseif ( isset($_POST['post_type']) && 'redes' == $_POST['post_type'] ) {
	// 	/* --- security verification --- */
	// 	if(!wp_verify_nonce($_POST['essay_final_nonce'], plugin_basename(__FILE__))) {
	// 		return $ID;
	// 	} // end if
			 
	// 	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
	// 		return $ID;
	// 	} // end if
			 
	// 	if('page' == $_POST['post_type']) {
	// 		if(!current_user_can('edit_page', $ID)) {
	// 		return $ID;
	// 		} // end if
	// 	} else {
	// 		if(!current_user_can('edit_page', $ID)) {
	// 			return $ID;
	// 		} // end if
	// 	} // end if
	// 	/* - end security verification - */
	// }

	 
	// Make sure the file array isn't empty
	if(!empty($_FILES['cover_picture_attachment']['name'])) {
		 
		// Setup the array of supported file types. In this case, it's just PDF.
		$supported_types = array('application/pdf', 'image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/png');
		 
		// Get the file type of the upload
		$arr_file_type = wp_check_filetype(basename($_FILES['cover_picture_attachment']['name']));
		$uploaded_type = $arr_file_type['type'];
		 
		// Check if the type is supported. If not, throw an error.
		if(in_array($uploaded_type, $supported_types)) {

			// Use the WordPress API to upload the file
			$upload = wp_upload_bits($_FILES['cover_picture_attachment']['name'], null, file_get_contents($_FILES['cover_picture_attachment']['tmp_name']));
	 
			if(isset($upload['error']) && $upload['error'] != 0) {
				wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
			} else {
				add_post_meta($ID, 'cover_picture_attachment', $upload);
				update_post_meta($ID, 'cover_picture_attachment', $upload);     
			} // end if/else

		} else {
			wp_die("The file type that you've uploaded is not a PDF.");
		} // end if/else
		 
	} // end if

	// Make sure the file array isn't empty
	if(!empty($_FILES['content_picture_attachment']['name'])) {
		 
		// Setup the array of supported file types. In this case, it's just PDF.
		$supported_types = array('application/pdf', 'image/jpeg', 'image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/png');
		 
		// Get the file type of the upload
		$arr_file_type = wp_check_filetype(basename($_FILES['content_picture_attachment']['name']));
		$uploaded_type = $arr_file_type['type'];
		 
		// Check if the type is supported. If not, throw an error.
		if(in_array($uploaded_type, $supported_types)) {

			// Use the WordPress API to upload the file
			$upload = wp_upload_bits($_FILES['content_picture_attachment']['name'], null, file_get_contents($_FILES['content_picture_attachment']['tmp_name']));
	 
			if(isset($upload['error']) && $upload['error'] != 0) {
				wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
			} else {
				add_post_meta($ID, 'content_picture_attachment', $upload);
				update_post_meta($ID, 'content_picture_attachment', $upload);     
			} // end if/else

		} else {
			wp_die("The file type that you've uploaded is not a PDF.");
		} // end if/else
		 
	} // end if

	if (!empty( $_POST['essay_final'] )) {
		$date_pieces  = explode(",", $_POST['essay_final']);
		$essay_format = "Todo " . $date_pieces[0] . ", às " . $date_pieces[1] . ":" . $date_pieces[2] . $date_pieces[3];

		// add_post_meta($ID, 'essay_final', $_POST['essay_final']);
		update_post_meta($ID, 'essay_final', $_POST['essay_final']);

		// add_post_meta($ID, 'essay_final_format', $essay_format);
		update_post_meta($ID, 'essay_final_format', $essay_format);
	}
	 
} // end save_custom_meta_data

function update_edit_form() {
	echo ' enctype="multipart/form-data"';
} // end update_edit_form

function remove_jquery_migrate( &$scripts) {
	if(!is_admin()) {
		$scripts->remove('jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
	}
}

function single_template_terms($template) {
	foreach( (array) wp_get_object_terms(get_the_ID(), get_taxonomies(array('public' => true, '_builtin' => false))) as $term ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$term->slug}.php") )
			return TEMPLATEPATH . "/single-{$term->slug}.php";
	}
	return $template;
}

function getPostViews($postID){
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
	}
	return $count.' Views';
}
function setPostViews($postID) {
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}

function page_m12( $template ) {
	global $post;

	if ( preg_match('/visao-celular/i', $post->post_name) ) {
		load_template( TEMPLATEPATH . '/page-visao-celular.php' );
	}
	return $template;
}

function get_the_content_obj( $more_link_text = null, $strip_teaser = false ) {
	global $page, $more, $preview, $pages, $multipage;

	$post = get_post();

	if ( null === $more_link_text )
			$more_link_text = __( '(more&hellip;)' );

	$output = '';
	$has_teaser = false;

	// If post password required and it doesn't match the cookie.
	if ( post_password_required( $post ) )
			return get_the_password_form( $post );

	if ( $page > count( $pages ) ) // if the requested page doesn't exist
			$page = count( $pages ); // give them the highest numbered page that DOES exist

	$content = $pages[$page - 1];
	if ( preg_match( '/<!--more(.*?)?-->/', $content, $matches ) ) {
			$content = explode( $matches[0], $content, 2 );
			if ( ! empty( $matches[1] ) && ! empty( $more_link_text ) )
					$more_link_text = strip_tags( wp_kses_no_null( trim( $matches[1] ) ) );

			$has_teaser = true;
	} else {
			$content = array( $content );
	}

	return $content;
}

function the_content_vars( $more_link_text = null, $strip_teaser = false) {
	$content = get_the_content_obj( $more_link_text, $strip_teaser );

	for ($i=0; $i < count($content); $i++) { 
		$content[$i] = apply_filters( 'the_content', $content[$i] );
		$content[$i] = str_replace( ']]>', ']]&gt;', $content[$i] );
	}
	return $content;
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {
 
	if (empty($pagerange)) {
		$pagerange = 2;
	}
 
	/**
	 * This first part of our function is a fallback
	 * for custom pagination inside a regular loop that
	 * uses the global $paged and global $wp_query variables.
	 * 
	 * It's good because we can now override default pagination
	 * in our theme, and use this function in default quries
	 * and custom queries.
	 */
	global $paged;
	if (empty($paged)) {
		$paged = 1;
	}
	if ($numpages == '') {
		global $wp_query;
		$numpages = $wp_query->max_num_pages;
		if(!$numpages) {
				$numpages = 1;
		}
	}
 
	/** 
	 * We construct the pagination arguments to enter into our paginate_links
	 * function. 
	 */
	$pagination_args = array(
		'base'            => get_pagenum_link(1) . '%_%',
		'format'          => 'page/%#%',
		'total'           => $numpages,
		'current'         => $paged,
		'show_all'        => false,
		'end_size'        => 1,
		'mid_size'        => $pagerange,
		'prev_next'       => True,
		'prev_text'       => __('<i class="icon-left-open-big"></i>'),
		'next_text'       => __('<i class="icon-right-open-big"></i>'),
		'type'            => 'plain',
		'add_args'        => false,
		'add_fragment'    => ''
	);

	$paginate_links = paginate_links($pagination_args);
	
	if ($paginate_links) {
		echo "<nav class='custom-pagination text-center'>";
			// echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
			echo $paginate_links;
		echo "</nav>";
	}
 
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 

add_filter('single_template', 'single_template_terms');
add_filter('page_template', 'page_m12');
add_filter('show_admin_bar', '__return_false');
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );

add_action('post_edit_form_tag', 'update_edit_form');
add_action('save_post', 'save_custom_meta_data');
add_action('wp_enqueue_scripts', 'theme_scripts');
add_action('add_meta_boxes', 'add_custom_meta_boxes');
add_action( 'init', 'wptp_create_post_type' );
add_action( 'init', 'wptp_register_taxonomies' );

add_theme_support('post-thumbnails', array('page','post', 'noticias', 'redes', 'lideranca', 'igrejas'));
add_image_size( 'large-thumb', 308, 200, true );
add_image_size( 'ministerio-thumb', 252, 130, true );
add_image_size( 'thumb-highlight', 650, 300, true );
add_image_size( 'event-highlight', 580, 245, true );

remove_action('wp_head', 'wp_generator');