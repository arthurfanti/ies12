				<?php
					if ( !in_array(get_post_type(), array("page", "post")) ) {
						$obj  = get_post_type_object( get_post_type( $post->ID ) );
						$obj  = $obj->labels;
						$name = $obj->name;
					} else {
						$name = $post->post_name;
					}

					switch ($name) {
						case 'Ministerios':
						case 'ministerios':
							$class = 'icon-ministerios greeny';
							$pageTitle = $name;
							break;
						case 'Noticias':
						case 'noticias':
							$class = 'icon-noticias blue';
							$pageTitle = 'Notícias';
							break;

						case 'Redes':
						case 'redes':
							$class = 'icon-redes blue';
							$pageTitle = $name;
							break;
						
						case 'Estudos de Célula':
						case 'estudos de célula':
						case 'Estudos de Discipulado':
						case 'estudos de discipulado':
							$class = 'icon-estudos deep_blue';
							$pageTitle = $name;
							break;


						case 'Historia':
						case 'historia':
							$class = 'icon-historia bluey';
							$pageTitle = $post->post_title;
							break;

						case 'Liderança':
						case 'liderança':
							$class     = 'icon-lideranca orange';
							$terms     = wp_get_object_terms($post->ID, get_taxonomies(array('public' => true, '_builtin' => false)));
							$terms     = $terms[0];
							$pageTitle = ( $terms->name == 'Apóstolo' ) ? 'Apóstolos' : 'Pastores' ;
							break;

						case 'Igrejas':
						case 'igrejas':
							$class     = 'icon-igrejas orange';
							$pageTitle = $name;
							break;

						case (preg_match('/visao-celular/', $name) ? true : false) :
							$class     = 'icon-visao deep_blue';
							$pageTitle = $post->post_title;
							break;

						case 'Eventos':
						case 'eventos':
							$class     = 'icon-calendario-bloco purple';
							$pageTitle = 'Agenda';
							break;

						default:
							# code...
							break;
					}
				?>
				<div class="page-header">
					<div class="row">
						<div class="small-12 columns">
							<h2 class="uppercase <?= $class; ?>"><?= $pageTitle; ?></h2>
						</div>
					</div>
					<div data-interchange="[<?= get_template_directory_uri() ?>/images/backgrounds/bg-noticias.jpg, (default)]" alt=""></div>
					<div class="panel">
						<div class="row">
							<div class="small-10 small-centered columns">
								<ul data-orbit data-options="bullets:false; slide_number:false; timer:false;">
								<?php $insiderNews_query = new WP_Query(array('post_type' => 'noticias', 'news-category' => 'por-dentro', 'posts_per_page' => 3, 'orderby' => 'id', 'order' => 'DESC')); ?>
								<?php if ( $insiderNews_query->have_posts() ) : while ( $insiderNews_query->have_posts() ) : $insiderNews_query->the_post(); ?>
									<!-- post -->
									<li>
											<a class="text-justify" href="<?= the_permalink(); ?>"><span><?= ( strlen(get_the_excerpt()) > 110 ) ? substr(get_the_excerpt(), 0, 110) . "..." : get_the_excerpt(); ?></span></a>
									</li>
								<?php endwhile; ?>
								<?php wp_reset_postdata(); ?>
								</ul>
									<!-- post navigation -->
								<?php else: ?>
									<span>Ooops! Nada para ver aqui, ainda :-(</span>
								<?php endif; ?>
							</div>
						</div>

					</div>
					<?php if ( is_post_type_archive( 'eventos' ) ): ?>
					<div class="purple block" style="margin-top:-1.25rem; margin-bottom:1.25rem">
						<div class="row">
							<form role="filterCalendar" action="<?= get_post_type_archive_link( 'eventos' ); ?>" method="get">
								<div class="row">
									<div class="small-1 columns">
										<strong>Filtrar por:</strong>
									</div>
									<div class="small-6 small-centered medium-3 medium-uncentered columns">
										<select name="mes" id="mes">
											<option value="01">Janeiro</option>
											<option value="02">Fevereiro</option>
											<option value="03">Março</option>
											<option value="04">Abril</option>
											<option value="05">Maio</option>
											<option value="06">Junho</option>
											<option value="07">Julho</option>
											<option value="08">Agosto</option>
											<option value="09">Setembro</option>
											<option value="10">Outubro</option>
											<option value="11">Novembro</option>
											<option value="12">Dezembro</option>
										</select>
									</div>
									<div class="small-6 small-centered medium-3 medium-uncentered columns">
										<select name="ano" id="ano">
											<option value="<?= date('Y', strtotime('-1 year')) ?>"><?= date('Y', strtotime('-1 year')) ?></option>
											<option selected value="<?= date('Y') ?>"><?= date('Y') ?></option>
											<option value="<?= date('Y', strtotime('+1 year')) ?>"><?= date('Y', strtotime('+1 year')) ?></option>
										</select>
									</div>
									<div class="small-2 columns end">
										<input type="submit" class="button tiny" value="Aplicar Filtro">
									</div>
								</div>
							</form>
						</div>
					</div>
					<?php endif ?>
				</div>