<?php
	get_header();

	setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');

	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

	if ( isset($_GET['mes']) || isset($_GET['ano']) ) {
		$args = array( 'post_type' => 'eventos', 'posts_per_page' => -1 );
	} else {
		$args = array( 'post_type' => 'eventos', 'posts_per_page' => 15, 'paged' => $paged );
	}

	$posts_array = new WP_Query( $args );
	// }
 ?>
		<div id="content" role="main">
			<section role="page" content="agenda">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row collapse">
					<div class="small-10 small-centered medium-12 columns" data-equalizer>
						<ul class="agenda small-block-grid-1 medium-block-grid-3">
							<?php if ( $posts_array->have_posts() ) : while ( $posts_array->have_posts() ) : $posts_array->the_post(); ?>
							<!-- post -->
							<?php
								$date = get_field('data');
								
								// extract Y,M,D
								$y    = substr($date, 0, 4);
								$m    = substr($date, 4, 2);
								$d    = substr($date, 6, 2);
								
								// create UNIX
								$time = strtotime("{$d}-{$m}-{$y}");
								$mes  = strftime('%b', $time);

								if ( isset($_GET['mes']) && !empty($_GET['mes']) ) {
									if ( $_GET['mes'] != $m ) {
										continue;
									}
								}

								if ( isset($_GET['ano']) && !empty($_GET['ano']) ) {
									if ( $_GET['ano'] != $y ) {
										continue;
									}
								}
							?>
							<li>
								<div class="data strong">
									<strong><?= $d ?></strong><?= $mes ?>
								</div>
								<span>
									<strong><?php the_title(); ?></strong>
									<small><?= get_field('local') ?></small>
								</span>
							</li>
							<?php endwhile; ?>
							<!-- post navigation -->
						</ul>
							<?php
								if (function_exists('custom_pagination') && !isset($_GET['mes']) && !isset($_GET['ano']) ) {
									custom_pagination($posts_array->max_num_pages,"",$paged);
								}
							?>
							
							<?php wp_reset_postdata(); ?>
							<?php else: ?>
							<!-- no posts found -->
							<?php endif; ?>
					</div>
				</div>
			</section>
		</div>
		
		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>