						<aside role="sidebar" class="contact-side">
							<div class="row collapse">
								<ul>
									<li><a href="#">Rua Mont'Alverne, 363 - Ipiranga <br> São Paulo - SP | Brasil</a></li>
									<li><a href="#">contato@ies.com</a></li>
									<li><a href="">+55 11 2068 4020 | 2068 9285</a></li>
								</ul>
								<h2>Redes Sociais</h2>
								<div role="social-media">
									<ul class="inline-list radius">
										<li><a href="#" class="social icon-youtube"></a></li>
										<li><a href="#" class="social icon-flicker"></a></li>
										<li><a href="#" class="social icon-googleplus-01"></a></li>
										<li><a href="#" class="social icon-facebook"></a></li>
										<li><a href="#" class="social icon-twitter"></a></li>
										<li><a href="#" class="social icon-instagram"></a></li>
									</ul>
								</div>
							</div>

						</aside>