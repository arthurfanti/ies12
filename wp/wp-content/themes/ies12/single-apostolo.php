<?php
	
	$facebookUri  = get_post_meta($post->ID, 'leader_facebook');
	$twitterUri   = get_post_meta($post->ID, 'leader_twitter');
	$instagramUri = get_post_meta($post->ID, 'leader_instagram');
	$args         = array( 'post_type' => 'lideranca' );
	$posts_array  = get_posts( $args );
	$feedObject   = new feedObject();
	$feedArray    = json_decode($feedObject->get_twitter_feed( $twitterUri[0] ));

	get_header();
 ?>
		<div id="content" role="main">	
			<section role="page" content="single-apostolo">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="medium-6 columns">
						<div class="row collapse">
							<div class="small-12 column">
								<h5 class="orange uppercase"><?= $post->post_title; ?></h5>
								<p class="text-justify"><?= $post->post_content; ?></p>
							</div>
						</div>
						<hr>
						<div class="row collapse">
							<div class="small-10 small-centered medium-12 columns">
								<div class="row collapse">
									<div class="small-12 medium-6 columns">
										<strong class="uppercase">contato</strong>
										<a href="#">www.loremipsum.com.br</a>
									</div>
									<div class="small-12 medium-6 columns">
										<ul class="inline-list">
											<li><a href="#" class="social icon-youtube"></a></li>
											<li><a href="<?= 'http://www.facebook.com/' . $facebookUri[0] ?>" class="social icon-facebook"></a></li>
											<li><a href="#" class="social icon-twitter"></a></li>
											<li><a href="#" class="social icon-instagram"></a></li>
										</ul>
									</div>
								</div>

								<hr>
								<div class="row collapse">
									<div class="small-12 columns">
										<strong class="uppercase">compartilhe</strong>
										<?php echo do_shortcode('[ssba]'); ?>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="medium-6 columns">
						<?php if (has_post_thumbnail()) the_post_thumbnail( 'full' ); ?>
					</div>
				</div>
			</section>

			<section>
				<div class="row">
					<ul data-orbit data-options="slide_number:false; navigation_arrows:false; timer:false;">
						<?php foreach ($feedArray as $feed): ?>
							<li><?= $feed->text; ?></li>
						<?php endforeach ?>
					</ul>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>